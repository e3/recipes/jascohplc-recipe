# jasco conda recipe

Home: "https://github.com/ISISComputingGroup/EPICS-Jsco4180/jasco"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: Jasco Pump handler
